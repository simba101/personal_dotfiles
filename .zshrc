# Lines configured by zsh-newuser-install
HISTFILE=~/.zsh/.zshfile
HISTSIZE=1024
SAVEHIST=1024
setopt nomatch
unsetopt appendhistory autocd beep extendedglob notify
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/simba/.zshrc'

autoload -U compinit && compinit
autoload -U promptinit && promptinit
autoload -U colors && colors
# End of lines added by compinstall

# A nice prompt for simba:
PROMPT="%{$fg_no_bold[white]%}[%{$fg_no_bold[green]%}%n%{$fg_bold[blue]%} %~ %{$fg_no_bold[white]%}]%{$fg_no_bold[green]%}$ %{$reset_color%}"

# My own aliases:
alias sponge=clear
alias pms='sudo pm-suspend'
alias ssn='sudo shutdown now'
alias sc='thunar /home/simba/Source\ Code'
alias ls='ls --color=auto'
alias lss='ls -Al'
alias cd..='cd ..'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
