" Do NOT behave like classic vi (use vim mode instead):
set nocompatible

" do the pathogen autoload of modules:
execute pathogen#infect()
set background=dark

"make sure that *all* the colors load properly:
if &t_Co >= 256 || has("gui_running")
  set guifont=Consolas
  colorscheme wombat
endif
if &t_Co > 2 || has("gui_running")
  syntax on
endif

" Here come the bool options:
set ignorecase                      " ignore case when searching
set smartcase                       " be smart when searching :)
set number
set hlsearch
set incsearch
set expandtab                       " use spaces, not tabs.
set smarttab                        " be smart when using tabs :)
set autoindent
set smartindent
set wrap
set autoread                        " read the file if changed outside vim.
set ruler                           " always show the current file position.

set noerrorbells                    " no annoying bells.
set novisualbell

set nobackup                        " do NOT use any annoying form
set nowb                            " of backup.
set noswapfile
" end of bool options


" Here come the non-bool (numeric) options:
set history=512
set tabstop=2                       " make the TAB work properly.
set shiftwidth=2                    " defines the 2-spaces indenting model. 
set so=7                            " 7 lines to the cursor when scrolling j/k.
set backspace=eol,start,indent      " make backspace more user-friendly.
set tabpagemax=20                   " max number of tabs (separate files) is 20
set colorcolumn=80                  " visual 80col line
highlight ColorColumn guibg=Black
" END OF THE NON-BOOL (NUMERIC) OPTIONS.

filetype plugin on
filetype indent on

" Here come the useful key mappings:
let mapleader = ","
let g:mapleader = ","
let maplocalleader = "\\"
set pastetoggle=<f5>

" Make j and k smart on *long* lines:
map j gj
map k gk

" Make H and L work as "super-h" and "super-l"
nnoremap H g^
nnoremap L g$ 

" Command for quick saving of the current buffer:
nnoremap <leader>w :w!<cr>

" Use [;] instead of [:] ( which is [shift-;] )
nnoremap ; :

" Commands for tab-switching
nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>
nnoremap <silent> <A-Left> :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
nnoremap <silent> <A-Right> :execute 'silent! tabmove ' . tabpagenr()<CR>

"commands for quick .vimrc editing - <leader>ev and <leader>sv
nnoremap <leader>ev :vsplit $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>

"commands for surrounding a word with various characters
nnoremap <leader>" viw<esc>a"<esc>hbi"<esc>lel
nnoremap <leader>' viw<esc>a'<esc>hbi'<esc>lel

" Move a line of text using ALT+[jk]
nmap <M-j> mz:m+<cr>`z
nmap <M-k> mz:m-2<cr>`z
vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z

" Toggle paste mode on and off
map <leader>pp :setlocal paste!<cr>

" use the autocompletion: C-N ard C-P
map! ^P ^[a. ^[hbmmi?\<^[2h"zdt.@z^Mywmx`mP xi
map! ^N ^[a. ^[hbmmi/\<^[2h"zdt.@z^Mywmx`mP xi

" END OF USEFUL KEY MAPPINGS

" Abbreviations for autoexpanding (in INSERT mode)
iabbrev @@ szczerbiakadam@gmail.com
iabbrev spam@ asz101.allegro@gmail.com
iabbrev ccopr Copyright 2014 Adam Szczerbiak, all rights reserved
iabbrev ccopl Copyleft (by simba), share on!
iabbrev asimbas Adam "simba" Szczerbiak

" insert filename w/o extension.
inoremap \fn <C-R>=expand("%:t:r")<CR> 

" java abbreviations:
iabbrev j_psvm public static void main( String[] args )<cr>{
iabbrev j_sop System.out.println( "" );<esc>3hi
iabbrev j_forl for( int i = 0; i <; i++ )<esc>6hi
iabbrev j_const public static final
iabbrev j_final public static final 
iabbrev j_sdco frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
iabbrev j_gcp Container c = getContentPane();
iabbrev j_pvap public void actionPerformed( ActionEvent ae )

" C/C++ abbreviations:
iabbrev c_unstd using namespace std;
" END OF ABBREVIATIONS

" Autocommands:
autocmd FileType python nnoremap <localleader>c I# <esc>
autocmd FileType java nnoremap <localleader>c I// <esc>
autocmd FileType make setlocal noexpandtab

" END OF AUTOCOMMANDS

" Visual mode pressing * or # searches for the current selection
" Super useful! From an idea by Michael Naumann
vnoremap <silent> * :call VisualSelection('f')<CR>
vnoremap <silent> # :call VisualSelection('b')<CR>

" Be a pro, do NOT use arrow keys in NORMAL, VISUAL or INSERT modes:
noremap <Up> <nop>
noremap <Down> <nop>
noremap <Left> <nop>
noremap <Right> <nop>
inoremap <Up> <nop>
inoremap <Down> <nop>
inoremap <Left> <nop>
inoremap <Right> <nop>
